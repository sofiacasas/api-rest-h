module.exports={
    ports: process.env.PORT || 3000,
    db: process.env.MONGODB || 'mongodb://localhost:27017/SD-CRUD',
    SECRET:'miclavesecretadetokenamodificarenproduccion',
    TOKEN_EXP_TIME: 7*24*60 //7 dias expresados en minutos
};